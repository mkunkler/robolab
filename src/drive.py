##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################

'''
PATHFINDING

ToDo:
 - Fehlertoleranz beim über Node fahren
 - PIDC nutzen
 - gerades anfahren auf Nodes

'''

import ev3dev.ev3 as ev3
import time
import math
import gyro

import pidcontroller

class Engine():
    '''
    Handling the engines
    '''
    def __init__(self, port):
        self.motor = ev3.LargeMotor(port)
        self.motor.reset()
        self.motor.command = 'run-direct'
        self.base_speed = 0
        self.speed_boost = 0
        self.position_change_time = time.time()
        self.old_position = self.motor.position

    def position_change(self):
        erg = self.motor.position - self.old_position
        self.old_position = self.motor.position
        self.position_change_time = time.time()
        return erg

    def average_velocity(self):
        erg = self.motor.position - self.old_position
        time_passed = time.time() - self.position_change_time
        self.position_change_time = time.time()
        self.old_position = self.motor.position
        return erg, time_passed

    def refresh_speed(self):
        self.speed = self.base_speed + self.speed_boost
        self.motor.duty_cycle_sp = self.speed

    def set_base_speed(self, amount):
        self.base_speed = amount
        self.refresh_speed()

    def change_base_speed(self, amount):
        self.base_speed += amount
        self.refresh_speed()

    def boost_speed(self, amount):
        self.speed_boost = amount
        self.refresh_speed()

    def stop(self):
        self.base_speed = 0
        self.speed_boost = 0
        self.refresh_speed()


class Drive:
    '''
    Makes the robot navigate along the path
    - working with edges and nodes

    ToDo:
      - is the information about how many edges each node has given by the server, or does the robot need to recon it?
    '''
    WHEEL_SEPERATION = 10.5
    WHEEL_DIAMETER = 5.75

    def __init__(self):
        self.pidc = pidcontroller.PIDController()
        self.engine_left = Engine('outA')
        self.engine_right = Engine('outD')
        self.gyro = gyro.Gyro()
        self.gyro.start_recording()
        self.last_direction = 0
        self.node_approach_angle = 0
        self.pos = [0, 0]

    def track_pos_change(self):
        avr = self.engine_right.average_velocity()
        vr = avr[0] / 360 * self.WHEEL_DIAMETER * math.pi / avr[1]
        avl = self.engine_left.average_velocity()
        vl = avl[0] / 360 * self.WHEEL_DIAMETER * math.pi / avl[1]
        t = (avr[1] + avl[1]) / 2
        direction = self.last_direction

        if vr - vl != 0 and t > 0:

            self.pos[0] = self.pos[0] + ((self.WHEEL_SEPERATION * (vr + vl)) / (2 * (vr - vl))) * (math.sin( (((vr - vl) * t) / (self.WHEEL_SEPERATION) + direction))- math.sin(direction))
            self.pos[1] = self.pos[1] + ((self.WHEEL_SEPERATION * (vr + vl)) / (2 * (vr - vl))) * (math.cos( (((vr - vl) * t) / (self.WHEEL_SEPERATION)) + direction) - math.cos(direction))
        self.last_direction = math.radians(self.gyro.get_degree())


    def move_forward(self, speed=10):
        '''

        '''
        self.track_pos_change()
        self.engine_left.set_base_speed(speed)
        self.engine_right.set_base_speed(speed)

    def go_forward(self, speed=10):
        self.track_pos_change()
        self.move_forward(speed)
        self.engine_left.boost_speed(0)
        self.engine_right.boost_speed(0)

    def move_left(self, speed=10):
        self.track_pos_change()
        self.engine_left.boost_speed(-speed)
        self.engine_right.boost_speed(speed)

    def move_right(self, speed=10):
        self.track_pos_change()
        self.engine_left.boost_speed(speed)
        self.engine_right.boost_speed(-speed)

    def turn_around(self, speed=20, direction='right'):
        #no pos change, since turn_around keeps on spot
        self.track_pos_change()
        if direction == 'right':
            self.engine_left.set_base_speed(speed)
            self.engine_right.set_base_speed(-speed)
        elif direction == 'left':
            self.engine_left.set_base_speed(-speed)
            self.engine_right.set_base_speed(speed)

    def stop(self):
        self.engine_left.stop()
        self.engine_right.stop()
        self.track_pos_change()

    def follow_edge(self):
        move_out_time = 0
        diff = 0
        restart = False
        self.move_forward(30)
        while not self.pidc.on_node():
            brightness = self.pidc.get_brightness()
            if restart:
                self.move_forward(30)
                restart = False
            if brightness == False:
                self.stop()
                time.sleep(0.2)
                restart = True
                continue
            if 0.3 < brightness and brightness < 0.7:
                move_out_time = 0
                self.move_forward(30)
            elif 0.3 >= brightness:
                if move_out_time == 0:
                    move_out_time = time.time()
                elif time.time() - move_out_time > 5:
                    diff = 5
                else:
                    diff = time.time() - move_out_time
                self.move_forward(10 - 2*diff)
                self.move_right(20)
            elif brightness >= 0.7:
                if move_out_time == 0:
                    move_out_time = time.time()
                elif time.time() - move_out_time > 5:
                    diff = 5
                else:
                    diff = time.time() - move_out_time
                self.move_forward(20 - 2*diff)
                self.move_left(15)
        self.stop()

    def shortest_turn_direction(self, angle):
        '''
        diff < 180 --> turn right
        diff > 180 --> turn left
        '''
        diff = self.gyro.degree_difference_pos(angle)
        if diff > 180:
            turn_direction = 'right'
        elif diff <= 180:
            turn_direction = 'left'
        return turn_direction

    def goto_angle(self, angle, turn_direction):
        print(angle)
        self.turn_around(speed=25, direction=turn_direction)
        diff = self.gyro.degree_difference(angle)
        while diff > 3:
            diff = self.gyro.degree_difference(angle)
        self.stop()
        return turn_direction

    def goto_edge(self, direction):
        turn_direction = self.shortest_turn_direction(direction)
        if turn_direction == 'left':
            self.goto_angle((direction + 30) % 360, turn_direction)
        elif turn_direction == 'right':
            self.goto_angle((direction - 30) % 360, turn_direction)

        #time.sleep(5)
        #start scanning in the area of the angle
        self.turn_around(speed=30, direction=turn_direction)
        diff = self.gyro.degree_difference(direction)
        on_blank_abort = False

        while diff <= 30:
            diff = self.gyro.degree_difference(direction)
            if self.pidc.on_node():
                self.go_forward(35)
                time.sleep(0.3)
                self.stop()
                self.turn_around(speed=30, direction=turn_direction)
            elif self.pidc.on_edge():
                if turn_direction == 'right':
                    #make sure the pidc is not leftside out!
                    on_blank_abort = True
                    time.sleep(0.5) #let him move a little more on the edge
                    break
                elif turn_direction == 'left':
                    self.stop()
                    self.move_right(30)
                    self.move_forward(20)
                    time.sleep(0.2)
                    self.stop()
                    return
        while on_blank_abort:
            if self.pidc.on_blank():
                self.stop()
                return


        #edge was not found, so give it another try with a slightly different angle
        if turn_direction == 'right':
            self.goto_edge(direction % 360)
        elif turn_direction == 'left':
            self.goto_edge(direction % 360)

    def find_edge(self):
        '''
        Tries to find an edge
        To be called on a blank position
        '''
        while not self.pidc.on_edge():
            self.move_left()
        self.stop()

    def find_node(self):
        '''

        '''
        while not self.pidc.on_node() and not self.pidc.on_edge():
            self.move_forward()
            self.move_left()
        if not self.pidc.on_node():
            self.follow_edge()

    def go_on_node(self):
        self.go_forward(30)
        time.sleep(0.2)
        while self.pidc.on_node():
            pass
        if self.pidc.LED_off(self.pidc.get_RGB()):
            self.stop()
            time.sleep(0.5)
            self.go_on_node()
        time.sleep(0.2)
        self.stop()

    def explore_node(self):
        '''
        puts leftest (on top 0) degree value of the edges into a list and returns the list
        '''
        print("explore")
        self.go_on_node()

        self.turn_around(30)
        start_deg = self.gyro.get_degree()
        self.node_approach_angle = start_deg % 360
        erg = []
        self.turn_around(30)
        restart = False
        while self.gyro.get_degree() < start_deg+360:
            if self.pidc.LED_off(self.pidc.get_RGB()):
                self.stop()
                time.sleep(0.5)
                restart = True
            if restart:
                self.turn_around(30)
                restart = False
            if self.pidc.on_node():
                self.move_forward(30)
                time.sleep(0.2)
                self.stop()
                self.turn_around(30)
                continue
            elif not self.pidc.on_edge():
                continue
            #check wheter edge is alrdy in erg
            current_angle = self.gyro.get_degree() % 360
            no_add = False
            for angle in erg:
                if (angle - current_angle) % 360 < 45 or (current_angle - angle) % 360 < 45:
                    no_add = True
            if not no_add:
                erg.append(current_angle)
        self.stop()

        return erg


