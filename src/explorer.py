##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################

import math
import node

class Explorer():
    '''
    Applies depth-search
    '''
    def __init__(self, driver):
        self.driver = driver
        self.start_position = (0,0)
        self.edge_list = []
        self.nodelist = [] #list of Node class instances

    def start(self):
        #self.driver.find_node()
        exploration = self.driver.explore_node()
        self.add_node_to_nodelist(exploration, False)
        #self.print_node_list()
        while self.next_node() != False:
            pass
            #self.print_node_list()
            #print(self.pathlist)

    def add_node_to_nodelist(self, exploration, previous_node):
        new_node = node.Node()
        new_node.pos[0] = round(self.driver.pos[0] / 40) * 40
        new_node.pos[1] = round(self.driver.pos[1] / 40) * 40
        self.driver.pos[0] = new_node.pos[0]
        self.driver.pos[1] = new_node.pos[1]
        approaching_degree = self.driver.gyro.get_degree() % 360
        #new_node.set_approaching_edge(new_node.approaching_edge(approaching_degree))
        new_node.exploration_to_node(exploration)
        #print("approaching degree: ", approaching_degree)
        #print("approaching edge: ", new_node.get_approaching_edge(approaching_degree))
        new_node.set_remote_node(new_node.get_approaching_edge(approaching_degree), previous_node)
        self.nodelist.append(new_node)

    def print_node_list(self):
        print('####  NODELIST  ###')
        for node in self.nodelist:
            print('-----------------')
            print(node)
            print(node.pos)
            print(node.edges)
            print(node.approaching_edge)
            print('-----------------')

    def node_on_pos(self):
        for i in range(len(self.nodelist)):
            if self.nodelist[i].is_on_node(self.driver.pos):
                return i
        #print("NOTHING ON THIS SPOT!!!")
        return False

    def explore_new_node(self, edge):
        '''


        '''
        #go further
        print("POSITION: ", self.driver.pos)
        print("GOTO EDGE: ", edge)

        self.driver.goto_edge(edge)
        self.driver.find_node()

        print("NODE ERREICHT; POSITION: ", self.driver.pos)

        current_degree = self.driver.gyro.get_degree() % 360


        #check for really new node?
        found_node = self.node_on_pos() #index of found node
        #print("Found node index: ", found_node)
        if type(found_node) != bool:
            #node already exists, put connection
            self.driver.go_on_node()
            approached_node = self.nodelist[found_node]
            self.last_node.set_remote_node(edge, approached_node)
            approaching_degree = approached_node.get_approaching_edge(current_degree)
            approached_node.set_remote_node(approaching_degree, self.last_node)
            self.driver.pos[0] = approached_node.pos[0]
            self.driver.pos[1] = approached_node.pos[1]
            self.edge_list.append(approaching_degree)
            self.print_node_list()
            #input("existing node, go further?")
        else:
            #node does not exist. Create new one.
            exploration = self.driver.explore_node()
            self.add_node_to_nodelist(exploration, self.last_node)
            self.last_node.set_remote_node(edge, self.nodelist[-1])
            self.edge_list.append(self.nodelist[-1].get_approaching_edge(current_degree))
            self.print_node_list()
            #input("new node, go further?")

    def go_previous_node(self):
        #go back
        move_back_angle = self.edge_list.pop()
        self.driver.goto_edge(move_back_angle)
        self.driver.find_node()
        self.driver.go_on_node()
        approached_node_index = self.node_on_pos()
        approached_node = self.nodelist[approached_node_index]
        self.driver.pos[0] = approached_node.pos[0]
        self.driver.pos[1] = approached_node.pos[1]

    def next_node(self):
        current_node = self.nodelist[self.node_on_pos()]
        #print(current_node.pos)
        #print(current_node.edges)
        explore_edge = current_node.edge_to_explore()
        self.last_node = current_node #save node before approaching to new one
        if type(explore_edge) is not bool:
            self.explore_new_node(explore_edge)
            return True
        elif len(self.edge_list) > 0:
            #input("go back?")
            self.go_previous_node()
            return True
        return False