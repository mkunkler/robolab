##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################

import time
import ev3dev.ev3 as ev3

class Gyro:
    def __init__(self):
        self.gs = ev3.GyroSensor()
        self.gs.mode = 'GYRO-ANG'

    def start_recording(self):
        self.gs.mode = 'GYRO-CAL'
        self.gs.mode = 'GYRO-ANG'

    def get_degree(self):
        try:
            v = self.gs.value()
        except:
            time.sleep(0.1)
            v = self.get_degree()
        finally:
            return v
        #return self.gs.value()

    def degree_difference(self, target_deg):
        self_degree = self.get_degree()
        #print(self_degree, target_deg, min((self_degree - target_deg) % 360, (target_deg - self_degree) % 360))
        return min((self_degree - target_deg) % 360, (target_deg - self_degree) % 360)

    def degree_difference_pos(self, target_degree):
        self_degree = self.get_degree()
        #print( -(target_deg - self_degree) % 360)
        return -(target_degree - self_degree) % 360