##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################


import ev3dev.ev3 as ev3

class PIDController:
    '''

    '''
    def __init__(self):
        self.cs = ev3.ColorSensor()
        self.cs.mode = 'RGB-RAW'

    def get_RGB(self):

        r,g,b = self.cs.bin_data('hhh')
        return r,g,b

    def LED_off(self, rgb):
        if rgb[1] < 10 and rgb[2] < 10:
            #print("##################################  AUSFALL LED!!!!!!!!  ###############")
            self.cs.mode = 'RGB-RAW'
            return True
        return False

    def get_brightness(self):
        rgb = self.get_RGB()
        if self.LED_off(rgb):
            return False
        return (rgb[0]+rgb[1]+rgb[2])/(255*3)

    def on_edge(self):

        r, g, b = self.get_RGB()
        if r < 100 and g < 100 and b < 100:
            #print("on_edge")
            return True
        else:
            return False

    def on_node(self):

        r, g, b = self.get_RGB()
        if self.LED_off((r, g, b)):
            return False
        if r > 100 and g < 100 and b < 100:
            #print("on_node")
            return True
        else:
            return False

    def on_blank(self):

        r, g, b = self.get_RGB()
        if self.LED_off((r, g, b)):
            return False
        if r >= 100 and g >= 100 and b >= 100:
            #print("on_blank")
            return True
        else:
            return False
