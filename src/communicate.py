##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################

import paho.mqtt.client as mqtt
import time

class Communicate():
    def __init__(self):
        self.id = "004"
        self.password = "5l4svVJ2ld"
        #self.id = "007"
        #self.password = "scf65Ac15"
        self.server_address = 'stream-006.inf.tu-dresden.de'
        self.port = 8883

        self.planet_subsribed = False

        self.client = mqtt.Client(client_id=self.id, clean_session=False, protocol=mqtt.MQTTv31)
        self.client.on_message = self.message
        self.client.username_pw_set(self.id, password=self.password)
        self.client.connect(self.server_address, port=self.port)

        self.subscribe()
        self.client.loop_start()
        self.ready()
        st = time.time()
        while time.time()-st < 2 and not self.planet_subsribed:
            pass
        if self.planet_subsribed == False:
            self.subscribe_planet("Kepler-"+self.id)

    def subscribe(self):
        self.client.subscribe("explorer/"+self.id, qos=1)

    def subscribe_planet(self, planet_name):
        self.client.subscribe("planet/"+planet_name)
        print("planet senden: "+planet_name)
    def ready(self):
        self.client.publish(topic='explorer/'+self.id, payload='ready', qos=1, retain=False)

    def send(self, msg):
        self.client.publish('explorer/'+self.id+' "'+msg+'"')

    def edges_to_send(self, nodelist):
        c = 1
        res = {}
        last = False
        for edge in nodelist.keys():
            if type(last) != bool:
                v = int((edge-last) / 60)
                if v > 0:
                    c += v
                    res[c] = edge
            else:
                res[1] = edge
            last = edge
        return res

    def message(self, client, data, message):
        m = message.payload.decode('utf-8')
        print(m)
        self.planet_subsribed = True
        self.subscribe_planet(m)