#!/usr/bin/env python3

##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################

import time
import drive
import explorer
import communicate

if __name__ == '__main__':
    driver = drive.Drive()
    driver.find_node()
    c = communicate.Communicate()
    #driver.follow_edge()

    e = explorer.Explorer(driver)
    e.start()
    print(driver.pos)
    #driver.goto_edge(driver.explore_node()[0])


    f = open("result.txt", "w")
    for node in e.nodelist:
        f.write(str(node))
        f.write(str(node.pos))
        f.write(str(node.edges))
        f.write("\r\n")
    f.close()