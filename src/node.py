##########################################
#          RoboLab - Gruppe 06           #
#----------------------------------------#
#    Katharina Letz -- Michel Kunkler    #
# https://bitbucket.org/mkunkler/robolab #
##########################################

import math

class Node():

    def __init__(self):
        self.pos = [0,0]
        self.edges = {} #{angle : connecting_node}
        self.approaching_edge = 0
        self.explored = False

    def exploration_to_node(self, exploration):
        for angle in exploration:
            self.edges[angle] = True

    def get_approaching_edge(self, approach_angle):
        '''
        Finds edge where the robot is approaching from.
        '''
        return self.find_edge_to_degree((approach_angle - 180) % 360)

    def find_edge_to_degree(self, approach_angle):
        '''
        finds next edge to degree
        '''
        min_dif = 360
        res = 0
        print("FIND EDGE TO FUCKING DEGREE: ", approach_angle)
        print("FIND EDGE TO DEGREE [EDGES]: ", self.edges)
        for angle in self.edges:
            dif = min((angle - approach_angle) % 360, (approach_angle - angle) % 360)
            if dif < min_dif:
                min_dif = min((angle - approach_angle) % 360, (approach_angle - angle) % 360)
                res = angle
        return res


    def is_on_node(self, pos):
        '''
        Takes position argument and determines, wheter this node is equal to the position.
        '''
        dif_x = math.fabs(pos[0] - self.pos[0])
        dif_y = math.fabs(pos[1] - self.pos[1])
        if dif_x <= 20 and dif_y <= 20:
            return True
        return False

    def edge_to_explore(self):
        '''
        Returns first unexplored edge on this node.
        '''
        for angle in self.edges:
            if self.edges[angle] == True:
                return angle
        return False


    def set_remote_node(self, angle, remote_node):
        '''
        Puts connection to other node in self.edges
        '''
        if remote_node != False:
            print("set remote node: ", angle, self.edges, remote_node.pos)
        else:
            print("set remote node: ", angle, self.edges, False)
        self.edges[angle] = remote_node